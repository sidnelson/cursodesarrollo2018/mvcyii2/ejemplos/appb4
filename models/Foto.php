<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "foto".
 *
 * @property int $id_foto
 * @property int $articulo
 * @property string $nombre
 * @property string $alt
 *
 * @property Articulo $articulo0
 */
class Foto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['articulo'], 'integer'],
            [['nombre', 'alt'], 'string', 'max' => 255],
            [['articulo', 'nombre'], 'unique', 'targetAttribute' => ['articulo', 'nombre']],
            [['articulo'], 'exist', 'skipOnError' => true, 'targetClass' => Articulo::className(), 'targetAttribute' => ['articulo' => 'id_articulo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_foto' => 'Id Foto',
            'articulo' => 'Articulo',
            'nombre' => 'Nombre',
            'alt' => 'Alt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticulo0()
    {
        return $this->hasOne(Articulo::className(), ['id_articulo' => 'articulo']);
    }
    
    
}
