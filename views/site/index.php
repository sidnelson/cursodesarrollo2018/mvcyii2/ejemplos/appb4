<?php

use app\models\Foto;
/* @var $this yii\web\View */

$this->title = 'listado';
?>
<div class="site-index">
    <ul>
        <?php
        foreach($articulos as $articulo){
            echo $this->render("index/_articulo",[
                "datos"=>$articulo
            ]);
            
            /** del articulo actual queiro ver las fotos */
            $fotosArticulo=$articulo->fotos; // es un array de modelos Foto
            
            foreach($fotosArticulo as $foto){
                echo $this->render("index/_fotos",[
                "datos"=>$foto,
            ]);
            }
           
            
            
            /*$a=Foto::find()
                    ->where(["articulo"=>$articulo->id_articulo])
                    ->all();
            var_dump($a);*/
        }
        ?>
    </ul>
</div>
