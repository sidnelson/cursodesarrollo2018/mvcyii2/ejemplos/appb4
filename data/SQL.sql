DROP DATABASE IF EXISTS appb4;
CREATE DATABASE appb4;
USE appb4;

DROP TABLE IF EXISTS articulo;
CREATE TABLE articulo (
  id_articulo int(11) AUTO_INCREMENT,
  titulo varchar(255),
  texto text,
  PRIMARY KEY (id_articulo)
  );

DROP TABLE IF EXISTS foto;
CREATE TABLE foto(
  id_foto int AUTO_INCREMENT,
  articulo int(11),
  nombre varchar(255),
  alt varchar(255),
  PRIMARY KEY (id_foto),
  UNIQUE KEY (articulo,nombre),
  CONSTRAINT FKfotoarticulo 
  FOREIGN KEY(articulo) REFERENCES articulo(id_articulo)
  );
