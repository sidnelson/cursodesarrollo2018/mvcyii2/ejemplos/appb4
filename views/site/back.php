<?php
use yii\helpers\Html;

?>


<ul class="nav nav-stacked">
  <li role="presentation"> <?= Html::a('Articulos', ['articulo/index'], ['class' => 'btn btn-info']) ?> </li>
  <li role="presentation"> <?= Html::a('Fotos', ['foto/index'], ['class' => 'btn btn-info']) ?></li>
</ul>